--vim.g.tokyonight_transparent_sidebar = true
--vim.g.tokyonight_transparent = true
--vim.opt.background = "dark"
--
--vim.cmd("colorscheme tokyonight")


vim.cmd("colorscheme lunaperche")

vim.g.gruvbox_transparent_bg=1
vim.opt.background = "dark"
--vim.api.nvim_set_hl(0, "ColorColumn", { ctermbg=0, bg=LightGrey })
vim.api.nvim_set_hl(0, "Normal", { ctermfg=White,  ctermbg=Black })

--vim.api.nvim_set_hl(0, 'Normal', { fg = "#ffffff", bg = "" })
--vim.api.nvim_set_hl(0, 'Comment', { fg = "#111111", bold = true })
--vim.api.nvim_set_hl(0, 'Error', { fg = "#ffffff", undercurl = true })
--vim.api.nvim_set_hl(0, 'Cursor', { reverse = true })
