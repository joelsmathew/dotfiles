-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  use 'folke/tokyonight.nvim'
  use 'gruvbox-community/gruvbox'

  use("nvim-telescope/telescope.nvim")
  use("nvim-lua/plenary.nvim")
  use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }

  use {
    'nvim-treesitter/nvim-treesitter',
    run = function()
        local ts_update = require('nvim-treesitter.install').update({ with_sync = true })
        ts_update()
    end,
    }

  use { "williamboman/mason.nvim" }
  use { "williamboman/mason-lspconfig.nvim"}
  use 'neovim/nvim-lspconfig' -- Configurations for Nvim LSP
  use 'mbbill/undotree'
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'saadparwaiz1/cmp_luasnip'
  use 'L3MON4D3/LuaSnip'
  use 'rafamadriz/friendly-snippets'
  use 'hrsh7th/cmp-nvim-lsp'

  use 'tpope/vim-fugitive'
  use 'shumphrey/fugitive-gitlab.vim'
end)
